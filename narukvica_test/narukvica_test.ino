/**
 *       ALGEBRA IoT school   
 *
 * @file       narukvica_test.ino
 *
 * @subsystem  
 *
 * @brief      Basic implementation of patient monitoring tracker functionality. 
 *
 * @author     Tomislav Razov    SmartSense, 2023
 *				
 *			   
 *				
 *			   Operation: measures temperature and battery voltage every few seconds and sends 
 *						  them to MQTT broker. When finger is placed on SpO2 sensor and held still,
 *						  pulse will be measured and sent. Red LED will indicate successfull pulse 
 *						  readout. 
 *
 */


#include <Arduino.h>
#include <Wire.h>
#include <SPI.h>
#include "Adafruit_MCP9808.h"
#include "MAX30105.h"
#include "heartRate.h"

MAX30105 pulseSensor;

#include <WiFi.h>
#include <PubSubClient.h>

//==================================================
// !!!IMPORTANT: CHANGE THIS TO YOUR DATA!!!
const char* ssid  = "stage1";
const char* pass = "stage123!";
const String student_name("tomo");
const char* mqtt_server = "smartsense.inet.hr";
const int   mqtt_port = 1883;
//==================================================

// MQTT topics strings
const String temperature_topic = String("algebra/iot/") + student_name + String("/temperature");
const String battery_topic = String("algebra/iot/") + student_name + String("/battery");
const String pulse_topic = String("algebra/iot/") + student_name + String("/pulse");
const String command_topic = String("algebra/iot/") + student_name + String("/command");
const String status_topic = String("algebra/iot/") + student_name + String("/status");


unsigned long mqtt_start_time;

long last = 0;
long last_pulse = 0;

WiFiClient client;
PubSubClient mqttClient(client);

const byte RATE_SIZE = 4; //Increase this for more averaging. 4 is good.
byte rates[RATE_SIZE]; //Array of heart rates
byte rateSpot = 0;
long lastBeat = 0; //Time at which the last beat occurred

float beatsPerMinute;
int beatAvg;


#define DEBUG // Uncomment for debug output to the Serial stream




// Interrupt pin
const byte oxiInt = 14; 	// pin connected to MAX30102 INT
const byte ledPin = 13; 	// LED PIN
const byte vbattPin = 35;	// VBAT voltage

// Create the MCP9808 temperature sensor object
Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();

uint32_t elapsedTime,timeStart;

void read_temperature();

void measure_pulse();

void connect_to_wifi() {
  
  Serial.println("Connecting to WiFi");
  
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("received IP address: ");
  Serial.println(WiFi.localIP());
}


void power_off_sensors() {
	
	Serial.println("Shutting down MSP9808...");
	tempsensor.shutdown_wake(1); // shutdown MSP9808 - power consumption ~0.1 mikro Ampere, stops temperature sampling
	//Serial.println("Shutting down MAX30102...");
	
}


// Callback used for MQTT subscriber client
void callback(char* topic, byte* payload, unsigned int length) {

	char buf [32];

	Serial.println("message received");
	Serial.print("  -> topic: ");
	Serial.println(topic);

	Serial.print("  -> message: ");

	int i;
  
	for (i = 0; i < length; i++) {
    
		Serial.print((char)payload[i]);
		buf[i] = (char)payload[i];
	}
  
	buf[i+1] = 0; //nul termination for string
	Serial.println();


	if (strncmp("sleep", buf, strlen("sleep")) == 0) {

		mqttClient.publish(status_topic.c_str(), "sleeping");
	}

	// go to power saving mode, you can wake the bracelet using "reset" button
	power_off_sensors();
  
	//hasta la vista bejbe!
	esp_deep_sleep_start();
}



void setup() {

	pinMode(oxiInt, INPUT);  	// pin 14 connects to the interrupt output pin of the MAX30102
	pinMode(vbattPin, INPUT);	// ADC to measure battery voltage
	pinMode(ledPin, OUTPUT);	// RED LED on standard PIN 13 - indicates successfull pulse measurement
	
	mqtt_start_time = millis();

	Wire.begin();
  
	Serial.begin(115200);
	
	mqttClient.setServer(mqtt_server, mqtt_port);
	mqttClient.setCallback(callback);
  
	if (!tempsensor.begin(0x18)) {
    
		Serial.println("Couldn't find MCP9808! Check your connections and verify the address is correct.");
		
	}
	else {
	
		Serial.println("Found MCP9808!");
		tempsensor.setResolution(3); // sets the resolution mode of reading, the modes are defined in the table bellow:
		// Mode Resolution SampleTime
		//  0    0.5°C       30 ms
		//  1    0.25°C      65 ms
		//  2    0.125°C     130 ms
		//  3    0.0625°C    250 ms
	
	
		Serial.println("wake up MCP9808.... "); // wake up MCP9808 - power consumption ~200 mikro Ampere
		tempsensor.wake();   // wake up, ready to read
	}
  if (!pulseSensor.begin(Wire, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
  {
    Serial.println("MAX30105 was not found. Please check wiring/power. ");
    while (1);
  }
  
  Serial.println("Place your index finger on the sensor with steady pressure.");

  pulseSensor.setup(); //Configure sensor with default settings
  pulseSensor.setPulseAmplitudeRed(0x0A); //Turn Red LED to low to indicate sensor is running
  pulseSensor.setPulseAmplitudeGreen(0); //Turn off Green LED
  
	timeStart=millis();
	
	connect_to_wifi();
}


// here we take care that our connection to MQTT broker is active and we maintain our subscription
void mqtt_handler() {

	static int count = 0;

	// We check the MQTT connection every 5 seconds  
	if (millis() - mqtt_start_time >= 5000) {

		mqtt_start_time = millis();
    
		if (!mqttClient.connected()) {

			if (mqttClient.connect((String("narukvca_") + student_name).c_str())) {
          
				mqttClient.subscribe(command_topic.c_str());
          
				Serial.println("Connected to mqtt broker");

			}
			else {
        
				Serial.println("Failed to connect to mqtt broker");
			} 
		}
	} 
  
	mqttClient.loop(); 
}


void read_temperature() {

	static float avg_temp = 0;
	static int cnt = 0;

	float c = tempsensor.readTempC();
	Serial.print("Temp: "); 
	Serial.print(c, 4); Serial.println(" oC"); 
	
	// we will send average temperatures
	avg_temp+=c;
	
	// 10 sec intervals
	if(cnt++ >= 10) {
	
		avg_temp/=cnt;
		mqttClient.publish(temperature_topic.c_str(), String(avg_temp).c_str());
		Serial.print("Average Temp: "); 
		Serial.print(avg_temp, 4); Serial.println(" oC"); 
		cnt = 0;
		avg_temp = 0;
	}	
}

void measure_batt_voltage() {
	
	static uint16_t avg_voltage = 0;
	static int cnt = 0;
	
	uint16_t voltage = analogRead(vbattPin);
	Serial.print("Vbatt: "); 
	Serial.print(voltage * 2); 
	Serial.println(" mV");
	
	avg_voltage+=voltage;
	
		// 10 sec intervals
	if(cnt++ >= 10) {
	
		avg_voltage/=cnt;
		mqttClient.publish(battery_topic.c_str(), String(avg_voltage * 2).c_str());
		Serial.print("Average voltage: "); 
		Serial.print(avg_voltage*2); Serial.println(" mV"); 
		cnt = 0;
		avg_voltage = 0;
	}	
}


void measure_pulse() {

  long irValue = pulseSensor.getIR();

  if (checkForBeat(irValue) == true)
  {
    //We sensed a beat!
    long delta = millis() - lastBeat;
    lastBeat = millis();

    beatsPerMinute = 60 / (delta / 1000.0);

    if (beatsPerMinute < 255 && beatsPerMinute > 20)
    {
      rates[rateSpot++] = (byte)beatsPerMinute; //Store this reading in the array
      rateSpot %= RATE_SIZE; //Wrap variable

      //Take average of readings
      beatAvg = 0;
      for (byte x = 0 ; x < RATE_SIZE ; x++)
        beatAvg += rates[x];
      beatAvg /= RATE_SIZE;
    }
  }



  if (irValue < 50000) {
    //Serial.print(" No finger?");
  }
  else {

      // sende pulse once per second when the finger is on
      long elapsed = millis() - last_pulse;

        if (elapsed >= 1000) {
         
            mqttClient.publish(pulse_topic.c_str(), String(beatAvg).c_str());
    
            Serial.print("IR=");
            Serial.print(irValue);
            Serial.print(", BPM=");
            Serial.print(beatsPerMinute);
            Serial.print(", Avg BPM=");
            Serial.print(beatAvg);
            Serial.println();

            last_pulse = millis();  
        }
  }

  
  
}


void loop() {

  long elapsed = millis() - last;

  if (elapsed >= 1000) {
    
    read_temperature();
    measure_batt_voltage();
    mqttClient.publish(status_topic.c_str(), "running");
    last = millis();  
  }
 
	measure_pulse();
	
  
	mqtt_handler();
}



void millis_to_hours(uint32_t ms, char* hr_str) {
	
	char istr[6];
	uint32_t secs,mins,hrs;
	secs=ms/1000; // time in seconds
	mins=secs/60; // time in minutes
	secs-=60*mins; // leftover seconds
	hrs=mins/60; // time in hours
	mins-=60*hrs; // leftover minutes
	itoa(hrs,hr_str,10);
	strcat(hr_str,":");
	itoa(mins,istr,10);
	strcat(hr_str,istr);
	strcat(hr_str,":");
	itoa(secs,istr,10);
	strcat(hr_str,istr);
}
